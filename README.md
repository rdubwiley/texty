# Texty: A demo for user authentication using wish and bubbletea

![](screenshots/about.gif)

## Overview 
Libraries used:
- [Wish](https://github.com/charmbracelet/wish): SSH server and bubbletea middleware
- [Bubbletea](https://github.com/charmbracelet/bubbletea): TUI/content windows
- [Huh](https://github.com/charmbracelet/huh): All the forms
- [Bubbles](https://github.com/charmbracelet/bubbles): For the list component
- [Lipgloss](https://github.com/charmbracelet/lipgloss): For the terminal styling
- [sqlc](https://github.com/sqlc-dev/sqlc): For all the database operations
- [sqlite](https://www.sqlite.org/): For the database

While not used I tried using [vhs](https://github.com/charmbracelet/vhs), but found the interaction with needing alt in the navbar to not work.

Homepage implements the main view, which is a navbar vertically joined with content. To reduce interferring with input needed for other apps, the navbar and content nav requires a command+key combination. I used "alt" for this to limit interferrence with other commands and shortcuts.

The content folder has all of our various views and the navbar. We instantiate these by giving a config that states the key used for navigating to that page. Additionally we list the user roles that should be able to access each content.

The auth folder gives a number of functions to check the ssh public key that is passed from the wish server. Basic idea is to use the public ssh keys for our user queries (instead of something like password authentication), so we need some functions to convert back and forth from the bytes object to plaintext.

The schema and query files are used by sqlc to generate the models folder, which is all of our database operations translated to golang.

The commands folder runs our database operations in a way that returns bubble tea commands. The bubble tea commands then return a bubble tea message that are used in the update functions of the homepage and content models.

I also added a manage folder (very similar to something like how django handles things over in python world).  The current main takes in a list of user configs and uploads them to the db if it doesn't exist. One could add a CLI to do db operations, but I found interacting with SQL to be easy enough. Definitely would advise expanding here for a more substantive app.

## Motivation
While both wish and bubbletea had excellent examples I did not see anything incorporating the ssh key into a bubbletea app. This project attempts to implement an example of what an SSH web app could look like.

## Terminal Web Apps???

Using SSH for authentication isn't as silly as it sounds and I found it much easer working with text and terminal output.

Being able to turn your TUIs into something shareable/monetizable is a big hurdle for these projects. Apps over SSH are one way to address this (in this case using charm's wish library with bubbe tea). Something to checkout over in Python world is what textual is building by putting TUIs on the web [here](https://github.com/Textualize/textual-web).

Overall, apps over SSH gives a very unique content delivery mechanism while having promising features such as being able to identify users without needing something like a password management system.

Some good examples I've found in this space are:
- Terminal.shop (ssh terminal.shop)
- [Soft serve](https://github.com/charmbracelet/soft-serve) (ssh git.charm.sh)

## Auth Flow

![](screenshots/full_auth_flow.gif)

General idea
- Public SSH not in database: need to register show register form
- Public SSH in database (basic user): access notes with upgrade form
- Public SSH in database (upgraded user): access both notes and more notes (no upgrade form)

Note that you need to add a .ssh folder with a keypair called "id_ed25519" local to your binary for authentication to work. This keypair is needed as the identity of the ssh server.

## Data Storage

This is using sqlc so you'll need to start up a sqlite database locally at db.db and run the schema to create the tables. From there the app should handle all the other data creation.

## Unresolved Issues

While I was able to get a systemd script to serve the binary, I was unable to get color when using systemd. I believe this is due to my systemd set up and not an issue with the wish setup. Any recommendations or help would be appreciated. I put the service file in the deploy folder.

## Contact Links
- [Twitter](https://twitter.com/rdubwiley)
- [Email me](mailto:wileyrya@gmail.com)
- Or leave an issue or merge request here on gitlab

