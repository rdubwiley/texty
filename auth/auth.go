package auth

import (
	"context"
	"database/sql"
	"strings"

	"github.com/charmbracelet/ssh"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/rdubwiley/texty/models"
	gossh "golang.org/x/crypto/ssh"
)

func parseKey(k string) ssh.PublicKey {
	parsed, _, _, _, _ := ssh.ParseAuthorizedKey([]byte(k))
	return parsed
}

func ConvertKey(k ssh.PublicKey) string {
	return strings.TrimSpace(string(gossh.MarshalAuthorizedKey(k)[:]))
}

func checkKey(u models.User, k ssh.PublicKey) bool {
	return ssh.KeysEqual(k, parseKey(u.PubKey))
}

func GetUser(sqlDbLoc string, pubKey string) (models.User, error) {
	db, err := sql.Open("sqlite3", sqlDbLoc)
	if err != nil {
		return models.User{}, err
	}
	ctx := context.Background()
	queries := models.New(db)
	return queries.GetUserByPubKey(ctx, pubKey)
}

func CheckIdentity(sqlDbLoc string, s ssh.Session) (models.User, error) {
	convertedKey := ConvertKey(s.PublicKey())
	return GetUser(sqlDbLoc, convertedKey)
}
