package commands

import (
	"context"
	"database/sql"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/ssh"
	"gitlab.com/rdubwiley/texty/auth"
	"gitlab.com/rdubwiley/texty/models"
)

const SqlDbLoc = "db.db"

type ReloadNoteMessage struct{}

type ReloadMorenoteMessage struct{}

type RedirectMessage struct {
	Tab int
}

type UpdateMessage struct {
	Id               int64
	DataType         string
	Mode             string
	GoodResult       bool
	ErrorDescription string
	RedirectIndex    int
}

func SendRedirect(index int) tea.Cmd {
	return func() tea.Msg {
		return RedirectMessage{index}
	}
}

func SendNoteReload() tea.Cmd {
	return func() tea.Msg {
		return ReloadNoteMessage{}
	}
}

func SendMorenoteReload() tea.Cmd {
	return func() tea.Msg {
		return ReloadMorenoteMessage{}
	}
}

func RunCreateUserCommand(name string, email string, publicKey ssh.PublicKey, authLevel string, redirectIndex int) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", redirectIndex}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			insertedUser, err := queries.CreateUser(ctx, models.CreateUserParams{
				Name:      name,
				Email:     sql.NullString{String: email, Valid: true},
				PubKey:    convertedKey,
				AuthLevel: authLevel,
			})
			if err != nil {
				return UpdateMessage{0, "user", "error", false, "Couldn't insert to database", redirectIndex}
			}
			return UpdateMessage{insertedUser.ID, "user", "create", true, "", redirectIndex}
		} else {
			return UpdateMessage{authUser.ID, "user", "error", false, "Key already exists", redirectIndex}
		}

	}
}

func RunUpdateUserRoleCommand(publicKey ssh.PublicKey, authLevel string, redirectIndex int) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", redirectIndex}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{0, "user.AuthLevel", "error", false, "User doesn't exist", redirectIndex}

		} else {
			err := queries.UpdateUserAuthLevel(ctx, models.UpdateUserAuthLevelParams{
				ID:        authUser.ID,
				AuthLevel: authLevel,
			})
			if err != nil {
				return UpdateMessage{authUser.ID, "user.AuthLevel", "error", false, "Couldn't update auth level", redirectIndex}
			}
			return UpdateMessage{authUser.ID, "user.AuthLevel", "update", true, "", redirectIndex}
		}

	}
}

func RunCreateNoteCommand(publicKey ssh.PublicKey, title string, content string) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", 0}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{authUser.ID, "error", "note", false, "Something is very wrong", 0}
		} else {
			insertedNote, err := queries.CreateNote(ctx, models.CreateNoteParams{
				UserID:  sql.NullInt64{authUser.ID, true},
				Title:   title,
				Content: sql.NullString{String: content, Valid: true},
			})
			if err != nil {
				return UpdateMessage{0, "note", "error", false, "Couldn't insert to database", 0}
			}
			return UpdateMessage{insertedNote.ID, "note", "create", true, "", 0}
		}
	}
}

func RunUpdateNoteCommand(publicKey ssh.PublicKey, id int64, title string, content string) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", 0}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{authUser.ID, "note", "error", false, "Something is very wrong", 0}
		} else {
			note, err := queries.GetNoteById(ctx, id)
			if err == nil && authUser.ID == note.UserID.Int64 {
				err := queries.UpdateNote(ctx, models.UpdateNoteParams{
					ID:      id,
					Title:   title,
					Content: sql.NullString{content, true},
				})
				if err != nil {
					return UpdateMessage{id, "note", "update", false, "Couldn't run delete", 0}
				}
				return UpdateMessage{id, "note", "update", true, "", 0}
			} else {
				return UpdateMessage{id, "note", "error", false, "Couldn't run delete", 0}
			}
		}
	}
}

func RunDeleteNoteCommand(publicKey ssh.PublicKey, id int64) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", 0}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{authUser.ID, "error", "note", false, "Something is very wrong", 0}
		} else {
			note, err := queries.GetNoteById(ctx, id)
			if err == nil && authUser.ID == note.UserID.Int64 {
				err := queries.DeleteNote(ctx, id)
				if err != nil {
					return UpdateMessage{id, "note", "delete", false, "Couldn't run delete", 0}
				}
				return UpdateMessage{id, "note", "delete", true, "", 0}
			} else {
				return UpdateMessage{id, "note", "error", false, "Couldn't run delete", 0}
			}
		}
	}
}

func RunCreateMorenoteCommand(publicKey ssh.PublicKey, title string, content string) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", 0}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{authUser.ID, "error", "morenote", false, "Something is very wrong", 0}
		} else {
			insertedNote, err := queries.CreateMorenote(ctx, models.CreateMorenoteParams{
				UserID:  sql.NullInt64{authUser.ID, true},
				Title:   title,
				Content: sql.NullString{String: content, Valid: true},
			})
			if err != nil {
				return UpdateMessage{0, "morenote", "error", false, "Couldn't insert to database", 0}
			}
			return UpdateMessage{insertedNote.ID, "morenote", "create", true, "", 0}
		}
	}
}

func RunUpdateMorenoteCommand(publicKey ssh.PublicKey, id int64, title string, content string) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", 0}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{authUser.ID, "morenote", "error", false, "Something is very wrong", 0}
		} else {
			note, err := queries.GetMorenoteById(ctx, id)
			if err == nil && authUser.ID == note.UserID.Int64 && authUser.AuthLevel == "upgraded" {
				err := queries.UpdateMorenote(ctx, models.UpdateMorenoteParams{
					ID:      id,
					Title:   title,
					Content: sql.NullString{content, true},
				})
				if err != nil {
					return UpdateMessage{id, "morenote", "update", false, "Couldn't run delete", 0}
				}
				return UpdateMessage{id, "morenote", "update", true, "", 0}
			} else {
				return UpdateMessage{id, "morenote", "error", false, "Couldn't run delete", 0}
			}
		}
	}
}

func RunDeleteMorenoteCommand(publicKey ssh.PublicKey, id int64) tea.Cmd {
	return func() tea.Msg {
		db, err := sql.Open("sqlite3", SqlDbLoc)
		if err != nil {
			return UpdateMessage{0, "user", "error", false, "Couldn't open database", 0}
		}
		ctx := context.Background()
		queries := models.New(db)
		convertedKey := auth.ConvertKey(publicKey)
		authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
		if err != nil {
			return UpdateMessage{authUser.ID, "error", "morenote", false, "Something is very wrong", 0}
		} else {
			note, err := queries.GetMorenoteById(ctx, id)
			if err == nil && authUser.ID == note.UserID.Int64 && authUser.AuthLevel == "upgraded" {
				err := queries.DeleteMorenote(ctx, id)
				if err != nil {
					return UpdateMessage{id, "morenote", "delete", false, "Couldn't run delete", 0}
				}
				return UpdateMessage{id, "morenote", "delete", true, "", 0}
			} else {
				return UpdateMessage{id, "morenote", "error", false, "Couldn't run delete", 0}
			}
		}
	}
}
