package content

//This is the "upgraded" content, which is essentially the same as notes
//Only difference is the commands and the table it points to
//Requires an "upgraded" auth level for the user

import (
	"context"
	"database/sql"
	"fmt"
	"slices"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/ssh"
	"gitlab.com/rdubwiley/texty/auth"
	"gitlab.com/rdubwiley/texty/commands"
	txtC "gitlab.com/rdubwiley/texty/commands"
	"gitlab.com/rdubwiley/texty/models"
)

type MorenotesModel struct {
	PubKey        ssh.PublicKey
	List          list.Model
	MorenotesList []models.Morenote
	DocStyle      lipgloss.Style
	HelpStyle     lipgloss.Style
	Width         int
	IsFormMode    bool
	IsCreating    bool
	IsRemoving    bool
	IsEditing     bool
	ActiveForm    *huh.Form
	currentPage   int
}

func getMorenoteById(publicKey ssh.PublicKey, id int64) (models.Morenote, error) {
	db, err := sql.Open("sqlite3", commands.SqlDbLoc)
	if err != nil {
		return models.Morenote{}, err
	}
	ctx := context.Background()
	queries := models.New(db)
	convertedKey := auth.ConvertKey(publicKey)
	authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
	if err != nil {
		return models.Morenote{}, err
	} else {
		note, err := queries.GetMorenoteById(ctx, id)
		if err != nil && authUser.ID == note.UserID.Int64 && authUser.AuthLevel == "upgraded" {
			return note, err
		}
		return note, err
	}
}

func getMorenotes(publicKey ssh.PublicKey) ([]models.Morenote, error) {
	db, err := sql.Open("sqlite3", commands.SqlDbLoc)
	if err != nil {
		return []models.Morenote{}, err
	}
	ctx := context.Background()
	queries := models.New(db)
	convertedKey := auth.ConvertKey(publicKey)
	authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
	if err != nil {
		return []models.Morenote{}, err
	} else {
		notes, err := queries.ListMorenotes(ctx, sql.NullInt64{authUser.ID, true})
		if err != nil {
			return []models.Morenote{}, err
		}
		return notes, nil
	}
}

func GetMorenotesCreationForm() *huh.Form {
	return huh.NewForm(
		huh.NewGroup(
			huh.NewInput().
				Key("title").
				Title("What's the title?").
				Validate(func(s string) error {
					if len(s) == 0 {
						return fmt.Errorf("Must put something here")
					}
					return nil
				}),
			huh.NewText().
				Key("content").
				Title("What's the note say?"),
			huh.NewConfirm().
				Title("Is this correct?").
				Validate(func(v bool) error {
					if !v {
						return fmt.Errorf("shift+tab to go back")
					}
					return nil
				}),
		),
	)
}

func GetMorenotesEditForm(title *string, content *string) *huh.Form {
	return huh.NewForm(
		huh.NewGroup(
			huh.NewInput().
				Key("title").
				Title("What's the title?").
				Value(title),
			huh.NewText().
				Key("content").
				Title("What's the note say?").
				Value(content),
			huh.NewConfirm().
				Title("You sure you want to remove?").
				Validate(func(v bool) error {
					if !v {
						return fmt.Errorf("Backspace to go back")
					}
					return nil
				}),
		),
	)
}

func GetMorenotesRemoveForm() *huh.Form {
	return huh.NewForm(
		huh.NewGroup(
			huh.NewConfirm().
				Title("Is this correct?").
				Validate(func(v bool) error {
					if !v {
						return fmt.Errorf("Backspace to go back")
					}
					return nil
				}),
		),
	)
}

func (m MorenotesModel) Init() tea.Cmd {
	return commands.SendMorenoteReload()
}

func ConvertMorenotesToListItems(notes []models.Morenote) []list.Item {
	listItems := []list.Item{}
	for _, note := range notes {
		listItems = append(listItems, item{title: note.Title, desc: note.Content.String})
	}
	return listItems
}

func (m MorenotesModel) HandleForms(commands []tea.Cmd, msg tea.Msg) (MorenotesModel, tea.Cmd) {
	form, cmd := m.ActiveForm.Update(msg)
	if f, ok := form.(*huh.Form); ok {
		m.ActiveForm = f
		if m.ActiveForm.State == huh.StateCompleted {
			m.IsFormMode = false
			if m.IsCreating {
				m.IsCreating = false
				newTitle := m.ActiveForm.GetString("title")
				newContent := m.ActiveForm.GetString("content")
				return m, txtC.RunCreateMorenoteCommand(m.PubKey, newTitle, newContent)
			}
			if m.IsEditing {
				newTitle := m.ActiveForm.GetString("title")
				newContent := m.ActiveForm.GetString("content")
				m.MorenotesList[m.List.Index()].Title = newTitle
				m.MorenotesList[m.List.Index()].Content = sql.NullString{newContent, true}
				newListItem := item{newTitle, newContent}
				m.List.SetItem(m.List.Index(), newListItem)
				//TODO implement server side change
				m.IsEditing = false
				return m, txtC.RunUpdateMorenoteCommand(m.PubKey, m.MorenotesList[m.List.Index()].ID, newTitle, newContent)
			}
			if m.IsRemoving {
				deleteId := m.MorenotesList[m.List.Index()].ID
				m.MorenotesList = slices.Delete(m.MorenotesList, m.List.Index(), m.List.Index()+1)
				//Pretty janky need to learn how the cursor moves
				m.List.RemoveItem(m.List.Index())
				if m.List.Index() != 0 {
					m.List.Select(m.List.Index() - 1)
				} else {
					m.List.Select(m.List.Index())
				}
				m.IsRemoving = false
				return m, txtC.RunDeleteMorenoteCommand(m.PubKey, deleteId)
			}
		}
	}
	return m, cmd

}

func (m MorenotesModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	commands := []tea.Cmd{}
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.List.SetSize(3*(msg.Width/4), 7*(msg.Height/10))
		m.Width = msg.Width
	case txtC.ReloadMorenoteMessage:
		notesList, err := getMorenotes(m.PubKey)
		slices.Reverse(notesList)
		if err == nil {
			m.MorenotesList = notesList
			m.List.SetItems(ConvertMorenotesToListItems(m.MorenotesList))
		}
	case txtC.UpdateMessage:
		if msg.DataType == "morenote" && msg.GoodResult {
			switch msg.Mode {
			case "create":
				//Need to get data because we don't know the note id
				newMorenote, err := getMorenoteById(m.PubKey, msg.Id)
				if err == nil {
					newTitle := newMorenote.Title
					newContent := newMorenote.Content
					m.MorenotesList = slices.Insert(m.MorenotesList, 0, newMorenote)
					newListItem := item{newTitle, newContent.String}
					m.List.InsertItem(0, newListItem)
					m.List.Select(0)
				}

			}

		}
	}
	if m.IsFormMode {
		m, cmd := m.HandleForms(commands, msg)
		commands = append(commands, cmd)
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "esc":
				m.IsFormMode = false
				m.IsCreating = false
				m.IsEditing = false
				m.IsRemoving = false
			case "ctrl+c":
				return m, tea.Quit
			}
		}
		return m, tea.Batch(commands...)
	} else {
		m.List, cmd = m.List.Update(msg)
		commands = append(commands, cmd)
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "ctrl+c":
				return m, tea.Quit
			case "enter":
				m.IsFormMode = true
				m.IsEditing = true
				note := m.MorenotesList[m.List.Index()]
				m.ActiveForm = GetMorenotesEditForm(&note.Title, &note.Content.String)
				cmd = m.ActiveForm.Init()
				commands = append(commands, cmd)
				return m, tea.Batch(commands...)
			case "c":
				m.IsFormMode = true
				m.IsCreating = true
				m.ActiveForm = GetMorenotesCreationForm()
				cmd = m.ActiveForm.Init()
				commands = append(commands, cmd)
				return m, tea.Batch(commands...)
			case "r":
				m.IsFormMode = true
				m.IsRemoving = true
				m.currentPage = m.List.Paginator.Page
				m.ActiveForm = GetMorenotesRemoveForm()
				cmd = m.ActiveForm.Init()
				commands = append(commands, cmd)
				return m, tea.Batch(commands...)
			}
		}
	}
	return m, tea.Batch(commands...)
}

func (m MorenotesModel) View() string {
	//TODO
	if m.IsFormMode {
		return lipgloss.JoinVertical(lipgloss.Top, m.DocStyle.Render(m.ActiveForm.View()), m.DocStyle.Render("Press ESC to go back to list"))
	}
	list := m.DocStyle.Render(m.List.View())
	help := m.DocStyle.PaddingBottom(1).Width(m.Width).Align(lipgloss.Center).Render("Press C to create, Enter to edit, R to remove")
	return lipgloss.JoinVertical(lipgloss.Top, help, list)
}
