package content

//This is the navbar model, which is the model which is viewed on top of the content (aka the boxes with the commands)

import (
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/rdubwiley/texty/commands"
)

type NavItem struct {
	Command string
	NavText string
}

type NavBarModel struct {
	NavKey           string
	ActiveTab        int
	NavItems         []NavItem
	AllNavItems      []NavItem
	NavBarStyle      lipgloss.Style
	CommandStyle     lipgloss.Style
	NavTextStyle     lipgloss.Style
	ActiveStyle      lipgloss.Style
	InactiveStyle    lipgloss.Style
	Width            int
	Height           int
	NavItemWidth     int
	UseActiveScaling bool
	CommandLookup    []string
	AllCommandLookup []string
}

// We hold all available content
// If something changes need to be able to update what we can see in the nav
func (m NavBarModel) UpdateNav(indices []int) NavBarModel {
	navItems := []NavItem{}
	commandLookup := []string{}
	for _, index := range indices {
		navItems = append(navItems, m.AllNavItems[index])
		commandLookup = append(commandLookup, m.NavKey+"+"+m.AllCommandLookup[index])
	}
	m.NavItems = navItems
	m.CommandLookup = commandLookup
	return m
}

func (m NavBarModel) Init() tea.Cmd {
	return nil
}

func (m NavBarModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {

	case commands.RedirectMessage:
		m.ActiveTab = msg.Tab
		return m, nil

	case tea.WindowSizeMsg:
		m.Height = msg.Height
		m.Width = msg.Width

	case tea.KeyMsg:
		for i, command := range m.CommandLookup {
			if msg.String() == command {
				m.ActiveTab = i
				return m, nil
			}
		}
	}

	return m, nil
}

func (m NavBarModel) View() string {
	//Generate a container that we are joining on the left
	output := lipgloss.JoinHorizontal(lipgloss.Left)
	var itemWidth int
	if m.Width < len(m.NavItems)*m.NavItemWidth {
		itemWidth = (m.Width/len(m.NavItems) - 2)
	} else {
		itemWidth = m.NavItemWidth
	}
	//For each nav item we join and reassign it to output
	for i, item := range m.NavItems {
		neededText := m.CommandStyle.Render(item.Command) + " " + m.NavTextStyle.Render(item.NavText)
		if i == m.ActiveTab {
			output = lipgloss.JoinHorizontal(lipgloss.Left, output, m.ActiveStyle.Width(itemWidth).Render(neededText))
		} else {
			output = lipgloss.JoinHorizontal(lipgloss.Left, output, m.InactiveStyle.Width(itemWidth).Render(neededText))
		}

	}
	//Use the width to determine how big this navbar should be
	if m.UseActiveScaling {
		return m.NavBarStyle.Width(m.Width).PaddingTop(2).Align(lipgloss.Center).Render(output)
	} else {
		return m.NavBarStyle.Render(output)
	}

}
