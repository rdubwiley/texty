package content

//This is the model for our notes which is our main content for this demo

import (
	"context"
	"database/sql"
	"fmt"
	"slices"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/ssh"
	"gitlab.com/rdubwiley/texty/auth"
	"gitlab.com/rdubwiley/texty/commands"
	txtC "gitlab.com/rdubwiley/texty/commands"
	"gitlab.com/rdubwiley/texty/models"
)

type item struct {
	title, desc string
}

func (i item) Title() string       { return i.title }
func (i item) Description() string { return i.desc }
func (i item) FilterValue() string { return i.title }

type NotesModel struct {
	PubKey      ssh.PublicKey
	List        list.Model
	NotesList   []models.Note
	DocStyle    lipgloss.Style
	HelpStyle   lipgloss.Style
	Width       int
	IsFormMode  bool
	IsCreating  bool
	IsRemoving  bool
	IsEditing   bool
	ActiveForm  *huh.Form
	currentPage int
}

// Don't want to get all the notes every time so just get the new one we need
func getNoteById(publicKey ssh.PublicKey, id int64) (models.Note, error) {
	db, err := sql.Open("sqlite3", commands.SqlDbLoc)
	if err != nil {
		return models.Note{}, err
	}
	ctx := context.Background()
	queries := models.New(db)
	convertedKey := auth.ConvertKey(publicKey)
	authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
	if err != nil {
		return models.Note{}, err
	} else {
		note, err := queries.GetNoteById(ctx, id)
		if err != nil && authUser.ID == note.UserID.Int64 {
			return note, err
		}
		return note, err
	}
}

// When we get a refresh message (usually on init) go get the notes
func getNotes(publicKey ssh.PublicKey) ([]models.Note, error) {
	db, err := sql.Open("sqlite3", commands.SqlDbLoc)
	if err != nil {
		return []models.Note{}, err
	}
	ctx := context.Background()
	queries := models.New(db)
	convertedKey := auth.ConvertKey(publicKey)
	authUser, err := queries.GetUserByPubKey(ctx, convertedKey)
	if err != nil {
		return []models.Note{}, err
	} else {
		notes, err := queries.ListNotes(ctx, sql.NullInt64{authUser.ID, true})
		if err != nil {
			return []models.Note{}, err
		}
		return notes, nil
	}
}

func GetNotesCreationForm() *huh.Form {
	return huh.NewForm(
		huh.NewGroup(
			huh.NewInput().
				Key("title").
				Title("What's the title?").
				Validate(func(s string) error {
					if len(s) == 0 {
						return fmt.Errorf("Must put something here")
					}
					return nil
				}),
			huh.NewText().
				Key("content").
				Title("What's the note say?"),
			huh.NewConfirm().
				Title("Is this correct?").
				Validate(func(v bool) error {
					if !v {
						return fmt.Errorf("shift+tab to go back")
					}
					return nil
				}),
		),
	)
}

func GetNotesEditForm(title *string, content *string) *huh.Form {
	return huh.NewForm(
		huh.NewGroup(
			huh.NewInput().
				Key("title").
				Title("What's the title?").
				Value(title),
			huh.NewText().
				Key("content").
				Title("What's the note say?").
				Value(content),
			huh.NewConfirm().
				Title("You sure you want to remove?").
				Validate(func(v bool) error {
					if !v {
						return fmt.Errorf("Backspace to go back")
					}
					return nil
				}),
		),
	)
}

func GetNotesRemoveForm() *huh.Form {
	return huh.NewForm(
		huh.NewGroup(
			huh.NewConfirm().
				Title("Is this correct?").
				Validate(func(v bool) error {
					if !v {
						return fmt.Errorf("Backspace to go back")
					}
					return nil
				}),
		),
	)
}

func (m NotesModel) Init() tea.Cmd {
	//Send a message to tell our app that it needs to go get the notes
	return commands.SendNoteReload()
}

// Helper function to get the list items from our notes list
func ConvertNotesToListItems(notes []models.Note) []list.Item {
	listItems := []list.Item{}
	for _, note := range notes {
		listItems = append(listItems, item{title: note.Title, desc: note.Content.String})
	}
	return listItems
}

// Our logic related to the form update
func (m NotesModel) HandleForms(commands []tea.Cmd, msg tea.Msg) (NotesModel, tea.Cmd) {
	form, cmd := m.ActiveForm.Update(msg)
	if f, ok := form.(*huh.Form); ok {
		m.ActiveForm = f
		if m.ActiveForm.State == huh.StateCompleted {
			m.IsFormMode = false
			if m.IsCreating {
				m.IsCreating = false
				newTitle := m.ActiveForm.GetString("title")
				newContent := m.ActiveForm.GetString("content")
				return m, txtC.RunCreateNoteCommand(m.PubKey, newTitle, newContent)
			}
			if m.IsEditing {
				newTitle := m.ActiveForm.GetString("title")
				newContent := m.ActiveForm.GetString("content")
				m.NotesList[m.List.Index()].Title = newTitle
				m.NotesList[m.List.Index()].Content = sql.NullString{newContent, true}
				newListItem := item{newTitle, newContent}
				m.List.SetItem(m.List.Index(), newListItem)
				//TODO implement server side change
				m.IsEditing = false
				return m, txtC.RunUpdateNoteCommand(m.PubKey, m.NotesList[m.List.Index()].ID, newTitle, newContent)
			}
			if m.IsRemoving {
				deleteId := m.NotesList[m.List.Index()].ID
				m.NotesList = slices.Delete(m.NotesList, m.List.Index(), m.List.Index()+1)
				//Pretty janky need to learn how the cursor moves
				m.List.RemoveItem(m.List.Index())
				if m.List.Index() != 0 {
					m.List.Select(m.List.Index() - 1)
				} else {
					m.List.Select(m.List.Index())
				}
				m.IsRemoving = false
				return m, txtC.RunDeleteNoteCommand(m.PubKey, deleteId)
			}
		}
	}
	return m, cmd

}

func (m NotesModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	commands := []tea.Cmd{}
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.List.SetSize(3*(msg.Width/4), 7*(msg.Height/10))
		m.Width = msg.Width
	//We got a reload message so reload the notes
	case txtC.ReloadNoteMessage:
		notesList, err := getNotes(m.PubKey)
		slices.Reverse(notesList)
		if err == nil {
			m.NotesList = notesList
			m.List.SetItems(ConvertNotesToListItems(m.NotesList))
		}

	//In case something changes for the notes
	//I only implemented create
	//delete and update are assumed to be good
	//We handle those by changing the state of the model
	//Data changes are pushed to the server by a command
	case txtC.UpdateMessage:
		if msg.DataType == "note" && msg.GoodResult {
			switch msg.Mode {
			case "create":
				//Need to get data because we don't know the note id
				newNote, err := getNoteById(m.PubKey, msg.Id)
				if err == nil {
					newTitle := newNote.Title
					newContent := newNote.Content
					m.NotesList = slices.Insert(m.NotesList, 0, newNote)
					newListItem := item{newTitle, newContent.String}
					m.List.InsertItem(0, newListItem)
					m.List.Select(0)
				}

			}

		}
	}
	if m.IsFormMode {
		m, cmd := m.HandleForms(commands, msg)
		commands = append(commands, cmd)
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "esc":
				m.IsFormMode = false
				m.IsCreating = false
				m.IsEditing = false
				m.IsRemoving = false
			case "ctrl+c":
				return m, tea.Quit
			}
		}
		return m, tea.Batch(commands...)
	} else {
		m.List, cmd = m.List.Update(msg)
		commands = append(commands, cmd)
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "ctrl+c":
				return m, tea.Quit
			//We're entering into edit mode for a note
			case "enter":
				m.IsFormMode = true
				m.IsEditing = true
				note := m.NotesList[m.List.Index()]
				m.ActiveForm = GetNotesEditForm(&note.Title, &note.Content.String)
				cmd = m.ActiveForm.Init()
				commands = append(commands, cmd)
				return m, tea.Batch(commands...)
			//We're entering into create mode
			case "c":
				m.IsFormMode = true
				m.IsCreating = true
				m.ActiveForm = GetNotesCreationForm()
				cmd = m.ActiveForm.Init()
				commands = append(commands, cmd)
				return m, tea.Batch(commands...)
			//We're entering into delete mode
			case "r":
				m.IsFormMode = true
				m.IsRemoving = true
				m.currentPage = m.List.Paginator.Page
				m.ActiveForm = GetNotesRemoveForm()
				cmd = m.ActiveForm.Init()
				commands = append(commands, cmd)
				return m, tea.Batch(commands...)
			}
		}
	}
	return m, tea.Batch(commands...)
}

func (m NotesModel) View() string {
	//Show the form
	if m.IsFormMode {
		return lipgloss.JoinVertical(lipgloss.Top, m.DocStyle.Render(m.ActiveForm.View()), m.DocStyle.Render("Press ESC to go back to list"))
	}
	//Show the list
	list := m.DocStyle.Render(m.List.View())
	help := m.DocStyle.PaddingBottom(1).Width(m.Width).Align(lipgloss.Center).Render("Press C to create, Enter to edit, R to remove")
	return lipgloss.JoinVertical(lipgloss.Top, help, list)
}
