package content

//This is the register form for new users

import (
	"fmt"
	"net/mail"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/ssh"
	"gitlab.com/rdubwiley/texty/commands"
)

type RegisterModel struct {
	form         *huh.Form // huh.Form is just a tea.Model
	pubKey       ssh.PublicKey
	docStyle     lipgloss.Style
	width        int
	explanation  string
	errorMessage string
}

func NewRegisterModel(pubKey ssh.PublicKey, style lipgloss.Style, width int, explanation string) RegisterModel {
	return RegisterModel{
		pubKey:      pubKey,
		docStyle:    style,
		width:       width,
		explanation: explanation,
		form: huh.NewForm(
			huh.NewGroup(
				huh.NewInput().
					Key("name").
					Title("What's your name?"),
				huh.NewInput().
					Key("email").
					Title("What's your email?").
					Validate(func(e string) error {
						_, err := mail.ParseAddress(e)
						if err != nil {
							return fmt.Errorf("You must supply an email")
						}
						return nil
					}),

				huh.NewConfirm().
					Title("Is this correct?").
					Validate(func(v bool) error {
						if !v {
							return fmt.Errorf("shift+tab to go back")
						}
						return nil
					}),
			),
		),
	}
}

func (m RegisterModel) Init() tea.Cmd {
	return m.form.Init()
}

func (m RegisterModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	form, cmd := m.form.Update(msg)
	switch msg.(type) {

	//Original idea was to try to go back to a blank form if waiting for data
	//Don't think this is needed anymore
	case commands.UpdateMessage:
		cmd = m.form.Init()
		m.form.State = huh.StateNormal
		return m, cmd
	}

	if f, ok := form.(*huh.Form); ok {
		m.form = f
		if m.form.State == huh.StateCompleted {
			//This command runs the db operations and gets back a message for the homepage to use
			return m, commands.RunCreateUserCommand(m.form.GetString("name"), m.form.GetString("email"), m.pubKey, "basic", 0)
		}
	}

	switch msg := msg.(type) {

	case tea.WindowSizeMsg:
		m.width = msg.Width
	}

	return m, cmd
}

func (m RegisterModel) View() string {
	if m.form.State == huh.StateCompleted {
		return m.docStyle.PaddingTop(2).Render("Redirecting you now")
	}
	return m.docStyle.Render(lipgloss.JoinVertical(lipgloss.Top, m.explanation+"\n", m.form.View()))

}
