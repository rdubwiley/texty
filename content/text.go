package content

import (
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/rdubwiley/texty/models"
)

type TextModel struct {
	User     models.User
	Width    int
	Text     string
	TxtStyle lipgloss.Style
}

func (m TextModel) Init() tea.Cmd {
	return nil
}

func (m TextModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {

	case tea.WindowSizeMsg:
		m.Width = msg.Width
	}

	return m, nil
}

func (m TextModel) View() string {
	return m.TxtStyle.Width(m.Width).Render(m.Text)
}
