package content

//This is the upgrade form to turn basic users into upgraded users

import (
	"fmt"
	"regexp"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/ssh"
	"gitlab.com/rdubwiley/texty/commands"
)

type CredCardModel struct {
	form        *huh.Form // huh.Form is just a tea.Model
	pubKey      ssh.PublicKey
	docStyle    lipgloss.Style
	width       int
	explanation string
}

func NewCreditCardModel(pubKey ssh.PublicKey, style lipgloss.Style, width int, explanation string) CredCardModel {
	return CredCardModel{
		pubKey:      pubKey,
		width:       width,
		docStyle:    style,
		explanation: explanation,
		form: huh.NewForm(
			huh.NewGroup(
				huh.NewInput().
					Key("name").
					Title("Name on the card"),
				huh.NewInput().
					Key("cardNumber").
					Title("Enter your credit card number").
					Password(true).
					Validate(func(e string) error {
						if e != "1111111111111111" {
							return fmt.Errorf("Enter 16 ones you are at length %d", len(e))
						}
						return nil
					}),
				huh.NewInput().
					Key("cvv").
					Title("Enter your CVV").
					Validate(func(e string) error {
						if len(e) != 3 {
							return fmt.Errorf("Must be a valid CVV (make something up)")
						}
						if !regexp.MustCompile(`^[0-9]+$`).MatchString(e) {
							return fmt.Errorf("Must be all numbers")
						}
						return nil
					}),

				huh.NewConfirm().
					Title("Is this correct?").
					Validate(func(v bool) error {
						if !v {
							return fmt.Errorf("shift+tab to go back")
						}
						return nil
					}),
			),
		),
	}
}

func (m CredCardModel) Init() tea.Cmd {
	return m.form.Init()
}

func (m CredCardModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	form, cmd := m.form.Update(msg)
	if f, ok := form.(*huh.Form); ok {
		m.form = f
		if m.form.State == huh.StateCompleted {
			cardNum := m.form.GetString("cardNumber")

			//The awesome data checking we're doing
			//Could implement something using the stripe API
			//Doesn't charge anyone, but could if imlpemented
			if cardNum == "1111111111111111" {
				//Updates the user role and sends a message for the homepage to refresh
				return m, commands.RunUpdateUserRoleCommand(m.pubKey, "upgraded", 1)
			}
		}
	}

	switch msg := msg.(type) {

	case tea.WindowSizeMsg:
		m.width = msg.Width
	}

	return m, cmd
}

func (m CredCardModel) View() string {
	style := lipgloss.NewStyle().Width(m.width).Align(lipgloss.Center)
	if m.form.State == huh.StateCompleted {
		return style.Render("Redirecting you now")
	}
	return style.Render(m.explanation+"\n") + style.Padding(1).Align(lipgloss.Center).Render(m.form.View())
}
