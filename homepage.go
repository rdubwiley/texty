package main

import (
	"slices"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/rdubwiley/texty/auth"
	textyCommands "gitlab.com/rdubwiley/texty/commands"
	"gitlab.com/rdubwiley/texty/content"
	"gitlab.com/rdubwiley/texty/models"
)

// A list of user roles for convenience
const UnauthenticatedRole = "Unathenticated"
const BasicRole = "basic"
const UpgradedRole = "upgraded"
const AllRole = "All"

type ContentConfig struct {
	name    string
	content tea.Model
	command string
	roles   []string
}

// Just a generic tea.Model to demo terminal information of ssh.
type HomepageModel struct {
	user           models.User
	NavBar         tea.Model
	NavBarConfig   content.NavBarModel
	currentContent int
	content        []tea.Model
	ContentConfigs []ContentConfig
	term           string
	width          int
	height         int
	txtStyle       lipgloss.Style
	quitStyle      lipgloss.Style
	commandList    []string
	registerIndex  int
}

// A helper function to construct the nav items needed for the navbar
func CreateNavConfig(navKey string, configs []ContentConfig) ([]string, []content.NavItem) {
	commands := []string{}
	navItems := []content.NavItem{}
	for _, config := range configs {
		commands = append(commands, config.command)
		navItems = append(navItems, content.NavItem{
			Command: navKey + "+" + config.command,
			NavText: config.name,
		})
	}
	return commands, navItems
}

func getCommand(commandKey string, command string) string {
	return commandKey + "+" + command
}

func (m HomepageModel) UpdateNav(indices []int) HomepageModel {
	m.NavBar = m.NavBarConfig.UpdateNav(indices)
	return m
}

// Gets back the content that can be accessed given our current auth levels
func (m HomepageModel) UpdateContent() (HomepageModel, []int) {
	commandList := []string{}
	content := []tea.Model{}
	indices := []int{}
	for i, config := range m.ContentConfigs {
		for _, role := range config.roles {
			switch role {
			case UnauthenticatedRole:
				if m.user.AuthLevel == "" {
					commandList = append(commandList, getCommand(m.NavBarConfig.NavKey, config.command))
					content = append(content, config.content)
					indices = append(indices, i)
				}
			case AllRole, m.user.AuthLevel:
				commandList = append(commandList, getCommand(m.NavBarConfig.NavKey, config.command))
				content = append(content, config.content)
				indices = append(indices, i)
			}
		}
	}
	m.commandList = commandList
	m.content = content
	return m, indices

}

func (m HomepageModel) NavUpdate(i int) (HomepageModel, tea.Cmd) {
	m.currentContent = i
	return m, nil
}

// These are the three functions we need to implement for a bubble tea app (Init(), Update(), and View())
func (m HomepageModel) Init() tea.Cmd {
	commands := []tea.Cmd{}
	for _, content := range m.content {
		commands = append(commands, content.Init())
	}
	return tea.Batch(commands...)
}

func (m HomepageModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	commands := []tea.Cmd{}
	m.content[m.currentContent], cmd = m.content[m.currentContent].Update(msg)
	commands = append(commands, cmd)
	switch msg := msg.(type) {

	//We need to update our notes
	//Send the message to all the content in case it's off screen
	case textyCommands.ReloadNoteMessage, textyCommands.ReloadMorenoteMessage:
		for i, _ := range m.content {
			m.content[i], cmd = m.content[i].Update(msg)
			commands = append(commands, cmd)
		}
		return m, tea.Batch(commands...)

	//Redirect message to a given content index
	case textyCommands.RedirectMessage:
		//Don't want to put us in a tab that doesn't exist
		if len(m.content) > msg.Tab {
			m.NavBar, _ = m.NavBar.Update(msg)
			m.currentContent = msg.Tab
		}

	//Some data has updated so need to deal with that
	case textyCommands.UpdateMessage:
		field := strings.Split(msg.DataType, ".")[0]
		switch field {

		case "user":
			if msg.GoodResult {
				//Something has changed for the user so need to reload it
				authUser, err := auth.GetUser(textyCommands.SqlDbLoc, m.user.PubKey)
				if err != nil {
					return m, nil
				}
				m.user = authUser
				commands = append(commands, textyCommands.SendRedirect(msg.RedirectIndex))
				//Need to update our content based on the changed auth level
				m, indices := m.UpdateContent()
				m = m.UpdateNav(indices)
				resizeMsg := tea.WindowSizeMsg{Height: m.height, Width: m.width}
				m.NavBar, _ = m.NavBar.Update(resizeMsg)
				for i, content := range m.content {
					m.content[i], _ = content.Update(resizeMsg)
					//I don't like reinitalizing here think of something better
					commands = append(commands, m.content[i].Init())
				}

				return m, tea.Batch(commands...)
			}
		}

	case tea.WindowSizeMsg:
		m.height = msg.Height
		m.width = msg.Width
		m.NavBar, _ = m.NavBar.Update(msg)
		for i, content := range m.content {
			m.content[i], _ = content.Update(msg)
		}

	case tea.KeyMsg:
		//Check for one of our nav commands
		if slices.Contains(m.commandList, msg.String()) {
			for i, command := range m.commandList {
				if command == msg.String() {
					m, _ = m.NavUpdate(i)
					m.NavBar, _ = m.NavBar.Update(msg)
				}
			}
		}
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit

		}
	}

	return m, tea.Batch(commands...)
}

func (m HomepageModel) View() string {
	return m.txtStyle.Align(lipgloss.Center).Render(lipgloss.JoinVertical(lipgloss.Top, m.NavBar.View(), m.content[m.currentContent].View()))
}
