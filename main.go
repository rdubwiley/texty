package main

//This serves the texty app
//I left most of the comments from the bubbletea example for clarity

import (
	"context"
	"errors"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
	"github.com/charmbracelet/ssh"
	"github.com/charmbracelet/wish"
	"github.com/charmbracelet/wish/activeterm"
	"github.com/charmbracelet/wish/bubbletea"
	"github.com/charmbracelet/wish/logging"
	"gitlab.com/rdubwiley/texty/auth"
	"gitlab.com/rdubwiley/texty/commands"
	"gitlab.com/rdubwiley/texty/content"
	"gitlab.com/rdubwiley/texty/models"
)

const (
	host = "localhost"
	port = "23234"
)

func main() {
	s, err := wish.NewServer(
		wish.WithAddress(net.JoinHostPort(host, port)),
		wish.WithHostKeyPath(".ssh/id_ed25519"),
		wish.WithPublicKeyAuth(func(ctx ssh.Context, key ssh.PublicKey) bool {
			return key.Type() == "ssh-ed25519"
		}),
		wish.WithMiddleware(
			bubbletea.Middleware(teaHandler),
			activeterm.Middleware(), // Bubble Tea apps usually require a PTY.
			logging.Middleware(),
		),
	)
	if err != nil {
		log.Error("Could not start server", "error", err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	log.Info("Starting SSH server", "host", host, "port", port)
	go func() {
		if err = s.ListenAndServe(); err != nil && !errors.Is(err, ssh.ErrServerClosed) {
			log.Error("Could not start server", "error", err)
			done <- nil
		}
	}()

	<-done
	log.Info("Stopping SSH server")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer func() { cancel() }()
	if err := s.Shutdown(ctx); err != nil && !errors.Is(err, ssh.ErrServerClosed) {
		log.Error("Could not stop server", "error", err)
	}
}

// You can wire any Bubble Tea model up to the middleware with a function that
// handles the incoming ssh.Session. Here we just grab the terminal info and
// pass it to the new model. You can also return tea.ProgramOptions (such as
// tea.WithAltScreen) on a session by session basis.
func teaHandler(s ssh.Session) (tea.Model, []tea.ProgramOption) {
	// This should never fail, as we are using the activeterm middleware.
	pty, _, _ := s.Pty()

	// When running a Bubble Tea app over SSH, you shouldn't use the default
	// lipgloss.NewStyle function.
	// That function will use the color profile from the os.Stdin, which is the
	// server, not the client.
	// We provide a MakeRenderer function in the bubbletea middleware package,
	// so you can easily get the correct renderer for the current session, and
	// use it to create the styles.
	// The recommended way to use these styles is to then pass them down to
	// your Bubble Tea model.
	renderer := bubbletea.MakeRenderer(s)
	quitStyle := renderer.NewStyle().Foreground(lipgloss.Color("8")).Padding(2)
	docStyle := renderer.NewStyle().Padding(2).Align(lipgloss.Center)

	//We check the user's public ssh key against the database
	authUser, err := auth.CheckIdentity(commands.SqlDbLoc, s)
	if err != nil {
		//We need the public key to register users
		authUser.PubKey = auth.ConvertKey(s.PublicKey())
	}
	//This is the form to register users that we haven't seen before
	//This can only be accessed if the key isn't in our database
	registerStyle := renderer.NewStyle().Align(lipgloss.Center)
	registerModel := content.NewRegisterModel(
		s.PublicKey(),
		registerStyle,
		pty.Window.Width,
		`
	Register Here!
	We need you to register before using this app.
		`,
	)
	//This is the model to upgrade from a basic account to an upgraded account
	//Instead of one notes section they get access to two
	upgradeModel := content.NewCreditCardModel(
		s.PublicKey(),
		renderer.NewStyle().PaddingTop(-10),
		pty.Window.Width,
		`
	One set of notes not good enough for you?
	Enter your info here and we'll give you a second set of notes!

	We don't store any of your credit card info.
		`,
	)
	//We need to set up the notes list to pass to our notes content model
	notesList := []models.Note{}
	notelistItems := content.ConvertNotesToListItems(notesList)
	notelist := list.New(notelistItems, list.NewDefaultDelegate(), 0, 0)
	notelist.SetShowHelp(false)
	notesModel := content.NotesModel{
		PubKey:     s.PublicKey(),
		List:       notelist,
		NotesList:  notesList,
		DocStyle:   docStyle,
		HelpStyle:  quitStyle,
		Width:      pty.Window.Width,
		IsFormMode: false,
		IsCreating: false,
		IsRemoving: false,
		IsEditing:  false,
	}
	notesModel.List.Title = "My notes"
	//morenotes is essentially just the notes content pointing towards a different table
	morenotesList := []models.Morenote{}
	morelistItems := content.ConvertMorenotesToListItems(morenotesList)
	morelist := list.New(morelistItems, list.NewDefaultDelegate(), 0, 0)
	morelist.SetShowHelp(false)
	morenotesModel := content.MorenotesModel{
		PubKey:        s.PublicKey(),
		List:          morelist,
		MorenotesList: morenotesList,
		DocStyle:      docStyle,
		HelpStyle:     quitStyle,
		Width:         pty.Window.Width,
		IsFormMode:    false,
		IsCreating:    false,
		IsRemoving:    false,
		IsEditing:     false,
	}
	morenotesModel.List.Title = "Even more notes!"
	txtStyle := renderer.NewStyle().
		Foreground(lipgloss.Color("10")).
		Align(lipgloss.Center)
	//Gives a block of text so good for an about section
	aboutModel := content.TextModel{
		User:  authUser,
		Width: pty.Window.Width,
		Text: `
	This is a demo app for the possibilities of using an ssh public key for user authentication.

	I've also added a credit card form (doesn't actually do card checking) to give an idea of how to hide content for various user roles.


	The repository for this demo project is: https://gitlab.com/rdubwiley/texty

	Feel free to leave an issue, a pull request, or star it if you like it.
		`,
		TxtStyle: txtStyle,
	}
	//This is a config to list the content, the user roles, and the command needed to navigate to it
	contentConfigs := []ContentConfig{
		{
			name:    "Register",
			content: registerModel,
			command: "r",
			roles:   []string{UnauthenticatedRole},
		},
		{
			name:    "Notes",
			content: notesModel,
			command: "1",
			roles:   []string{BasicRole, UpgradedRole},
		},
		{
			name:    "Morenotes",
			content: morenotesModel,
			command: "2",
			roles:   []string{UpgradedRole},
		},

		{
			name:    "Upgrade",
			content: upgradeModel,
			command: "u",
			roles:   []string{BasicRole},
		},
		{
			name:    "About",
			content: aboutModel,
			command: "a",
			roles:   []string{AllRole},
		},
	}
	//This is the navbar, which needs a command to prevent key colission
	navKey := "alt"
	commandList, navItems := CreateNavConfig(navKey, contentConfigs)
	navBar := content.NavBarModel{
		NavKey:       navKey,
		ActiveTab:    0,
		AllNavItems:  navItems,
		NavBarStyle:  renderer.NewStyle().Width(pty.Window.Width).Align(lipgloss.Center),
		CommandStyle: lipgloss.NewStyle().Bold(true),
		NavTextStyle: lipgloss.NewStyle(),
		ActiveStyle: lipgloss.NewStyle().
			Bold(true).
			Align(lipgloss.Center).
			BorderStyle(lipgloss.DoubleBorder()).
			BorderForeground(lipgloss.Color("10")),
		InactiveStyle: lipgloss.NewStyle().
			Align(lipgloss.Center).
			BorderStyle(lipgloss.NormalBorder()).
			BorderForeground(lipgloss.Color("63")),
		UseActiveScaling: true,
		NavItemWidth:     20,
		AllCommandLookup: commandList,
	}
	//Finally we bring everything into the homepage model, which is the main driver of the app
	m := HomepageModel{
		user:           authUser,
		term:           pty.Term,
		NavBarConfig:   navBar,
		ContentConfigs: contentConfigs,
		width:          pty.Window.Width,
		height:         pty.Window.Height,
		txtStyle:       txtStyle,
		quitStyle:      quitStyle,
		commandList:    commandList,
	}
	m, indices := m.UpdateContent()
	m = m.UpdateNav(indices)

	return m, []tea.ProgramOption{tea.WithAltScreen()}
}
