package main

import (
	"context"
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/rdubwiley/texty/models"
)

func createUsers() {
	var Users = []models.User{
		{1, "Ryan", sql.NullString{"ryan@test.com", true}, "<PUB SSH key here>", "basic"},
		//Put more stuff here
	}
	ctx := context.Background()

	db, err := sql.Open("sqlite3", "db.db")
	if err != nil {
		panic(err)
	}
	queries := models.New(db)
	for _, user := range Users {
		queryUser, err := queries.GetUserByPubKey(ctx, user.PubKey)
		if err != nil {
			newUser, err := queries.CreateUser(ctx, models.CreateUserParams{
				user.Name,
				user.Email,
				user.PubKey,
				user.AuthLevel,
			})
			if err != nil {
				panic(err)
			}
			fmt.Printf("Created User %s \n", newUser.Name)
		} else {
			fmt.Printf("Already have user %s skipping\n", queryUser.Name)
		}

	}

}

func main() {
	createUsers()
}
