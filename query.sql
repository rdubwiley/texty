-- name: GetUserByPubKey :one
SELECT * FROM users
WHERE pub_key = ? LIMIT 1;

-- name: GetUserById :one
SELECT * FROM users
WHERE id = ? LIMIT 1;

-- name: GetUserByEmail :one
SELECT * FROM users
WHERE email = ? LIMIT 1;

-- name: ListUsers :many
SELECT * FROM users
ORDER BY id;

-- name: CreateUser :one
INSERT INTO users (
  name, email, pub_key, auth_level
) VALUES (
  ?, ?, ?, ?
)
RETURNING *;

-- name: UpdateUserPubKey :exec
UPDATE users
set pub_key = ?
WHERE id = ?;

-- name: UpdateUserAuthLevel :exec
UPDATE users
set auth_level = ?
WHERE id = ?;

-- name: DeleteAuthorById :exec
DELETE FROM users
WHERE id = ?;

-- name: DeleteAuthorByPubKey :exec
DELETE FROM users
WHERE pub_key = ?;

-- name: CreateNote :one
INSERT INTO notes (
  user_id, title, content
) VALUES (
  ?, ?, ?
)
RETURNING *;

-- name: ListNotes :many
SELECT * FROM notes
WHERE user_id = ?
ORDER BY id;

-- name: GetNoteById :one
SELECT * FROM notes
WHERE id = ? LIMIT 1;

-- name: UpdateNote :exec
UPDATE notes
set title = ?,
  content = ?
WHERE id = ?;

-- name: DeleteNote :exec
DELETE FROM notes
WHERE id = ?;

-- name: CreateMorenote :one
INSERT INTO morenotes (
  user_id, title, content
) VALUES (
  ?, ?, ?
)
RETURNING *;

-- name: GetMorenoteById :one
SELECT * FROM morenotes
WHERE id = ? LIMIT 1;


-- name: ListMorenotes :many
SELECT * FROM morenotes
WHERE user_id = ?
ORDER BY id;

-- name: UpdateMorenote :exec
UPDATE morenotes
set title = ?,
  content = ?
WHERE id = ?;

-- name: DeleteMorenote :exec
DELETE FROM morenotes
WHERE id = ?;