CREATE TABLE users (
  id   INTEGER PRIMARY KEY,
  name text    NOT NULL,
  email text,
  pub_key text NOT NULL,
  auth_level text NOT NULL
);

CREATE TABLE notes (
    id INTEGER PRIMARY KEY,
    user_id INTEGER,
    content text
);

CREATE TABLE morenotes (
    id INTEGER PRIMARY KEY,
    user_id INTEGER,
    content text
);

ALTER TABLE notes ADD COLUMN title text NOT NULL;
ALTER TABLE morenotes ADD COLUMN title text NOT NULL;
